#!/bin/bash
export TF_VAR_region=${TF_VAR_region-'us-east1-b'}
export TF_VAR_name=${TF_VAR_name-'vpn-server'}
terraform apply -auto-approve
gcloud compute ssh --zone=${TF_VAR_region} ${TF_VAR_name} --command='echo init'
gcloud compute scp --zone=${TF_VAR_region} script.exp openvpn-install.sh wrapper.sh ${TF_VAR_name}:/home/$(whoami)/
gcloud compute ssh --zone=${TF_VAR_region} ${TF_VAR_name} --command='bash wrapper.sh'
gcloud compute scp --zone=${TF_VAR_region} ${TF_VAR_name}:/home/$(whoami)/client.ovpn .
