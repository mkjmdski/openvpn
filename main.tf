terraform {
  backend "gcs" {
    prefix  = "terraform/vpn"
    bucket  = "tfstates-bucket"
    project = "admin-devops-infra"
  }
}

provider "google" {
  credentials = "${file("creds.json")}"
  project     = "admin-devops-infra"
  region      = "us-east1"
}

resource "google_compute_address" "static" {
  name = "ipv4-address"
}

resource "google_compute_instance" "vpn-server" {
  name         = "${var.name}"
  machine_type = "n1-standard-1"
  zone         = "${var.region}"

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
    }
  }

  network_interface {
    network = "default"

    access_config {
      nat_ip = "${google_compute_address.static.address}"
    }
  }

  service_account {
    scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/monitoring.write",
    ]
  }
}

resource "google_compute_firewall" "vpn" {
  name    = "vpn-udp"
  network = "${google_compute_instance.vpn-server.network_interface.0.network}"

  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "udp"
    ports    = ["1194"]
  }
}
