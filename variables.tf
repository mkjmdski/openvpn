variable "region" {
  description = "Region for vpn vps"
  type        = "string"
  default     = "us-east1-b"
}

variable "name" {
  description = "name for vps"
  type        = "string"
  default     = "vpn-server"
}
